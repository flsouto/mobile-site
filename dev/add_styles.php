<?php

$paths = glob(__DIR__."/../*.html");
$paths = array_merge($paths, glob(__DIR__."/../*/*.html"));
foreach($paths as $path){
    $contents = file_get_contents($path);
    $fname = str_replace('.html','',basename($path));
    $link = '<link href="'.$fname.'.css" type="text/css" rel="stylesheet" />';
    if(!strstr($contents, $link)){
        $contents = str_replace('</body>','<link href="'.$fname.'.css" type="text/css" rel="stylesheet" /></body>', $contents);
        file_put_contents($path, $contents);
    }
    if(!file_exists($f=dirname($path).'/'.$fname.'.css')){
        file_put_contents($f,"");
    }
}
