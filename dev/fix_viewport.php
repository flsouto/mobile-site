<?php

$paths = glob(__DIR__."/../*.html");
$paths = array_merge($paths, glob(__DIR__."/../*/*.html"));
foreach($paths as $path){
    $contents = file_get_contents($path);
    $find = '<meta name="viewport" content="width=980"/>';
    $replace = '<meta name="viewport" content="width=device-width, initial-scale=1.0" />';
    if(strstr($contents,$find)){
        $contents = str_replace($find, $replace, $contents);
        file_put_contents($path, $contents);
    }

}
