# Mobile Site

Tornar todas as páginas html responsivas inserindo apenas código css em suas respectivas folhas de estilo. Não é permitido
alterar a estrutura das páginas, ou seja, mexer no HTML. Nenhum framework de frontend é permitido, apenas css puro.
